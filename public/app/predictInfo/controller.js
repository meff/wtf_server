
app.directive('predictInfo', function ($compile, $interpolate, $timeout) {
  var controller = function ($scope, $base64, $http) {
    $scope.photo = null;
    $scope.input = "";
    $scope.output = "";
    $scope.output_raw = {};

    $scope.json = function(value){
      var str = JSON.stringify(value, undefined, 3);
      return str;
    }

    var data =  { 
        image: "<BASE64>", 
        imageExtension: "png", 
        location: { 
            coords: { 
                latitude: 132.386862, 
                longitude: -12.127119 
            },
            timestamp: 1488925246017 
        } 
    };

    $scope._params = JSON.stringify(data, undefined, 3);
    $scope.endpoint = window.location.origin;

    $scope.file_changed = function (element) {
        $scope.$apply(function (scope) {
            $scope.photo = element.files[0];
        });
    };

    $scope.predictClick = function () {
        if (!$scope.photo) return;

        var reader = new FileReader();
        reader.onload = function () {
            var bytes = reader.result;
            callPredict(bytes);

        };
        reader.readAsBinaryString($scope.photo);
    }


    function callPredict(bytes) {

        data.image = $base64.encode(bytes);

        updateLogInput(data);

        $http.post('/api/predict_info', data).then(function (success) {
            updateLogOutput(success.data);
        }, function (error) {
            console.error(error);
        });

    }


    function updateLogInput(res) {
        $scope.$apply(function (scope) {
            $scope.input = JSON.stringify(res, undefined, 3);
        });
    };
    function updateLogOutput(res) {
         $scope.output_raw = res;
        $scope.output = JSON.stringify(res, undefined, 3);
    };
   
  }

  var directive = {
      restrict: 'E',
      scope: {
          title : '=?',
          actions : '=?'
      },
      templateUrl: "app/predictInfo/template.html",
      controller: controller
  };
  return directive;
});

