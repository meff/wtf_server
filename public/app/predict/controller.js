
app.directive('predict', function ($compile, $interpolate, $timeout) {
  var controller = function ($scope, $base64, $http) {
    $scope.photo = null;
    $scope.input = "";
    $scope.output = "";
    $scope.output_raw = {};

    $scope.json = function(value){
      var str = JSON.stringify(value, undefined, 3);
      return str;
    }

    var data = {
      base64: "<BASE64>"
    };
    $scope._params = JSON.stringify(data, undefined, 3);
    $scope.endpoint = window.location.origin;

    $scope.file_changed = function (element) {
        $scope.$apply(function (scope) {
            $scope.photo = element.files[0];
        });
    };

    $scope.predictClick = function () {
        if (!$scope.photo) return;

        var reader = new FileReader();
        reader.onload = function () {
            var bytes = reader.result;
            callPredict(bytes);

        };
        reader.readAsBinaryString($scope.photo);
    }


    function callPredict(bytes) {

        var prms = {
            base64: $base64.encode(bytes)
        };


        updateLogInput(prms);

        $http.post('/api/predict', prms).then(function (success) {
            console.log(success)
            updateLogOutput(success.data);
        }, function (error) {
            console.error(error);
        });

    }


    function updateLogInput(res) {
        $scope.$apply(function (scope) {
            $scope.input = JSON.stringify(res, undefined, 3);
        });
    };
    function updateLogOutput(res) {
         $scope.output_raw = res;
        $scope.output = JSON.stringify(res, undefined, 3);
    };
   
  }

  var directive = {
      restrict: 'E',
      scope: {
          title : '=?',
          actions : '=?'
      },
      templateUrl: "app/predict/template.html",
      controller: controller
  };
  return directive;
});

