
app.directive('fishInfo', function ($compile, $interpolate, $timeout) {
  var controller = function ($scope, $base64, $http) {
    $scope.photo = null;
    $scope.input = "";
    $scope.output = "";
    $scope.output_raw = {};
    $scope.fish_id = "<fish_id>";
    $scope.image_filename = "<image_filename>";
    
    
    var data = {
        id : "<FISH_ID>"
    }

    $scope._params = JSON.stringify(data, undefined, 3);
    $scope.endpoint = window.location.origin;



    function updateLogInput(res) {
        $scope.$apply(function (scope) {
            $scope.input = JSON.stringify(res, undefined, 3);
        });
    };
    function updateLogOutput(res) {
         $scope.output_raw = res;
        $scope.output = JSON.stringify(res, undefined, 3);
    };

    $scope.action = function () {
        if (!$scope.id) return;

      $http.post('/api/fishinfo', {id:$scope.id}).then(function (success) {
            if(success && success.data && success.data.output && success.data.output.imgs  && success.data.output.imgs.length > 0){
                $scope.fish_id = $scope.id;
                $scope.image_filename = success.data.output.imgs[0].filename;
            }
            updateLogOutput(success.data);
        }, function (error) {
            console.error(error);
        });

    }

   
  }

  var directive = {
      restrict: 'E',
      scope: {
          title : '=?',
          actions : '=?'
      },
      templateUrl: "app/fishInfo/template.html",
      controller: controller
  };
  return directive;
});

