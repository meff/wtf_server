
var app = angular.module('app', ['base64']);

app.controller('mainCtrl', function ($scope, $base64, $http) {
    $scope.id = "";
    $scope.input = "";
    $scope.output = "";

 
    $scope.action = function () {
        if (!$scope.id) return;

      $http.post('/api/fishinfo', {id:$scope.id}).then(function (success) {
            console.log(success)
            updateLogOutput(success.data);
        }, function (error) {
            console.error(error);
        });

    }



    function updateLogOutput(res) {
        $scope.output = JSON.stringify(res);
    };


});