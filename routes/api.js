var Clarifai = require('clarifai');
const cfapi = new Clarifai.App({
  apiKey: 'ab8a1b393e1740988f799a8b97b4f1ce'
 });

var getArgs = require('get-args');
var args = getArgs();
var api_id = args.options.api || 'fish-v2-dev';

var express = require('express');
var router = express.Router();
var fs = require('fs');

var multer = require('multer')
var upload = multer({ dest: './uploads/' });

router.post('/upload', upload.single('file'), function (req, res, next) {
  console.log(req.body);
  console.log(req.file);
  res.send(req.body);
})

router.post('/predict', function (req, res, next) {
  var base64 = req.body.base64;

  // cfapi.workflow.predict('fish-v2-dev', { base64: base64 }).then(
  cfapi.models.predict(api_id, { base64: base64 }).then(
    function (response) {
      res.send(_cfResponse(response, 0, function (conp) {
            return conp;
      }));
    },
    function (err) {
      res.send(err);
    }
  );
});

router.post('/fishinfo', function (req, res, next) {
  try {
    var id = req.body.id;
    var obj = JSON.parse(fs.readFileSync('./public/datas/' + id + '/info.json', 'utf8'));
    res.send(_response(obj, 0));
  }
  catch (ex) {
    res.send(_response({}, 10001, "No data", null));
  }
});


router.post('/predict_info', function (req, res, next) {
  var base64 = req.body.image;
  try {
    if (req.body.image && req.body.imageExtension && (req.body.imageExtension == "png" || req.body.imageExtension == "jpg" || req.body.imageExtension == "jpeg")) {
      cfapi.models.predict(api_id, { base64: base64 }).then(
        function (response) {
          res.send(_cfResponse(response, 0, function (conp) {
            if(conp.value < 0.20){
                return null;
            }
            try {
              
              var obj = JSON.parse(fs.readFileSync('./public/datas/' + conp.id + '/info.json', 'utf8'));
              obj.id = conp.id;
              for (var i in obj.imgs) {
                var img = obj.imgs[i];

                img.url = req.protocol + '://' + req.get('host') + "/datas/" + conp.id + "/" + img.filename;

              }
              
              return obj;
            } catch (ex) {
              return null;
            }
          }));
        },
        function (err) {
          res.send(err);
        }
      );
    }
    else if (!req.body.image) {
      res.send(_response({}, 10002, "Request have not image stream", null));
    }
    else if (!req.body.imageExtension || !(req.body.imageExtension == "png" || req.body.imageExtension == "jpg" || req.body.imageExtension == "jpeg")) {
      res.send(_response({}, 10003, "Request support png or jpg format", null));
    }
    else {
      res.send(_response({}, 1, "Unknow error", null));
    }
  }
  catch (ex) {
    res.send(_response({}, 10001, "No data", null));
  }
});


function _cfResponse(res, code, onReplaceConcept) {
  var cf_status = res.status;
  var outputs = [];
  if (res.outputs) {
    for (var i in res.outputs) {
      var output = res.outputs[i];

      if (output.data && output.data.concepts) {
        for (var j in output.data.concepts) {
          var concept = output.data.concepts[j];
          if (onReplaceConcept) {
            concept = onReplaceConcept(concept);
          }
          if (concept) {
            outputs.push(concept);
          }

          return _response(outputs, code, null, null, cf_status);
        }
      }

    }
  }

  return _response(outputs, code, null, null, cf_status);
}

function _response(output, code, errmsg, err, external_status) {
  var msg = "";
  switch (code) {
    //case 1: msg = errmsg; break;
    case 0: msg = "Success"; break;
    default: code = code || -1; msg = errmsg || "Error";
  }

  return {
    status: {
      external_status: external_status,
      code: code,
      msg: msg,
      err: err
    },
    output: output
  };
}


module.exports = router;
